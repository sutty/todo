# Kanban barato con notificaciones

Necesita configurar el daemon `atd` y tener un daemon de notificaciones
instalado y funcionando (no hay que hacer mucho, en mi caso tengo
`xfce4-notifyd`)

Cada archivo `.md` es una tabla Kanban, se abren en orden alfabético.

Cada línea es una tarea, si empezamos la línea con una fecha inteligible
por `at` (separada por punto y coma de la tarea), podemos programarla
utilizando algo así en `~/.vimrc`:

```
let mapleader=","
" Pasar la línea como entrada de at.sh y deshacer los cambios
noremap <leader>m :.!~/Projects/TODO/at.sh<CR>u
```

Luego, en una línea como esta, ejecutar `<leader>m` (en mi caso `,m`)

```
next week; estamos al aire
```

Los signos de admiración van a ser interpretados por la terminal así que
menos no usarlos :(

Cuando se haga el momento, vamos a recibir una notificación y un mail
por si no la vemos.
