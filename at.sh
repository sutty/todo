#!/bin/bash

minutes=10
millis="$(echo ${minutes}*60*1000 | bc)"
options=("-u critical" "-i task-due" "-t ${millis}" "-a TODO")

IFS=';'
while read -r when message; do
  echo "notify-send ${options[@]} TODO \"${message}\"" \
    | at -m "${when}"
done
