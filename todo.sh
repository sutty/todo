#!/bin/bash

cur="$(readlink -f "$0")"
dir="$(dirname "${cur}")"

cd "${dir}"

nvim -O *.md
